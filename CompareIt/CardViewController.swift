//
//  CardViewController.swift
//  CompareIt
//
//  Created by kkedziora on 26/11/2018.
//  Copyright © 2018 Home. All rights reserved.
//

import UIKit

class CardViewController: UIViewController {

    @IBOutlet weak var cardView: CardView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var scoreLabel: UILabel!

    var image: UIImage?
    var score: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        cardView.delegate = self
    }
}

extension CardViewController: CardViewDelegate {
    func cardGoesLeft(card: CardView) {
        dismiss(animated: true, completion: nil)
    }

    func cardGoesRight(card: CardView) {
        dismiss(animated: true, completion: nil)
    }

    func cardDiscarded(card: CardView) {
        dismiss(animated: true, completion: nil)
    }

    func currentCardStatus(card: CardView, distance: CGFloat) {

    }
}
