//
//  MainPageViewController.swift
//  CompareIt
//
//  Created by kkedziora on 25/11/2018.
//  Copyright © 2018 Home. All rights reserved.
//

import UIKit

class MainPageViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

}

extension MainPageViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let presentedViewController = CardViewController.instantiateFromStoryboard(UIStoryboard(name: "Main", bundle: Bundle(for: MainPageViewController.self)))

        UIApplication.shared.keyWindow?.rootViewController?.present(presentedViewController, animated: true, completion: nil)
    }
}

extension MainPageViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MainComparePhotosCell", for: indexPath)
        return cell
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
}
